const discord = require ('discord.js');
const fs = require ('fs');
const crypto = require ('crypto');
const https = require ('https');
const { exec } = require ('child_process');

const config = require ('./config.json');

const client = new discord.Client ({intents: ['GUILDS', 'GUILD_MESSAGES']});

client.on ('error', console.error);
client.on ('warn', console.warn);

client.on ('ready', () => {
    console.log ('ready');
});

const allowedExtensions = [ 'jpg', 'png', 'jpeg' ];

const getExtension = (filename) => {
    const index = filename.lastIndexOf ('.');
    return filename.substr (index + 1).toLowerCase ();
};

const downloadFile = (url, filename) => {
    return new Promise ((resolve, reject) => {
        const fileStream = fs.createWriteStream (filename);
        https.get (url, (res) => {
            res.pipe (fileStream);
            res.on ('end', () => {
                fileStream.close ();
                resolve (filename);
            });
        }, (error) => {
            reject (error);
        });
    });
};

const lickImage = (filePath, outputPath) => {
    return new Promise ((resolve, reject) => {
        exec (`magick convert -size 828x609 xc:black ${filePath} -distort Perspective "0,0,16,117 %w,0,470,75 %w,%h,508,405 0,%h,104,566" -composite licker.png -geometry +0+0 -composite ${outputPath}`, (error, stdout, stderr) => {
            if (error) {
                reject ([error, stderr]);
            }
            resolve ([stdout, stderr]);
        });
    });
};

client.on ('messageCreate', async (message) => {
    if (message.content === '!poliż') {
        const attachment = message.attachments.first ();
        if (!attachment) return;

        // attachment is there, begin processing
        if (attachment.size > config.maxFileSize) {
            message.reply ('ten obrazek jest zbyt duży');
            return;
        }

        const name = attachment.name;
        const url = attachment.url;

        if (allowedExtensions.indexOf (getExtension (name)) === -1) {
            message.reply ('niepoprawny typ pliku');
            return;
        }

        const uuid = crypto.randomBytes (16).toString ('hex');
        const fileName = `${uuid}.png`;
        const outputFileName = `${uuid}_out.png`;
        const filePath = `./tmp/${fileName}`;
        const outputFilePath = `./tmp/${outputFileName}`;

        try {
            await downloadFile (url, filePath);
            await lickImage (filePath, outputFilePath);

            message.reply ({ files: [ outputFilePath ] })
        } catch (error) {
            console.error (error);
            message.reply ('błąd 😎');
        }
    }
});

client.login (config.authToken);